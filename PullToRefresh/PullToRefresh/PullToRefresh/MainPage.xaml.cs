﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PullToRefresh
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        const int RefreshDuration = 1;
        bool isRefreshing;
        public ObservableCollection<Contact> Contacts { get; private set; }
        public ICommand RefreshCommand => new Command(async () => await RefreshItemsAsync());
        public MainPage()
        {
            Contacts = new ObservableCollection<Contact>()
        {
            new Contact()                               //
            {                                            //
                Name = "Jean",                           //
                Number = "+1 (702) 861-6102",            // Add new class contacts in Contacts ObservableCollection
                Favorite = true,                         //
                ImgContact = "baseline_account_circle_black_24dp.png",                         //
                Bio = "Tall blond with blue eyes"                         //
            },
        new Contact()
            {
                Name = "Steve Jobs",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "American business magnate, industrial designer, investor, and media proprietor. He was the chairman, chief executive officer (CEO), and co-founder of Apple Inc., the chairman and majority shareholder of Pixar, a member of The Walt Disney Company's board of directors following its acquisition of Pixar, and the founder, chairman, and CEO of NeXT. Jobs is widely recognized as a pioneer of the personal computer revolution of the 1970s and 1980s, along with Apple co-founder Steve Wozniak."
            },
            new Contact()
            {
                Name = "Alan Turing",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "English[6] mathematician, computer scientist, logician, cryptanalyst, philosopher, and theoretical biologist.[7] Turing was highly influential in the development of theoretical computer science, providing a formalisation of the concepts of algorithm and computation with the Turing machine, which can be considered a model of a general-purpose computer.[8][9][10] Turing is widely considered to be the father of theoretical computer science and artificial intelligence.[11] Despite these accomplishments, he was not fully recognised in his home country during his lifetime, due to his homosexuality, and because much of his work was covered by the Official Secrets Act."
            },
            new Contact()
            {
                Name = "Steve Wozniak",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "American electronics engineer, programmer, philanthropist, and technology entrepreneur. In 1976 he co-founded Apple Inc., which later became the world's largest information technology company by revenue and largest company in the world by market capitalization. Through their work at Apple in the 1970s and 1980s, he and Apple co-founder Steve Jobs are widely recognized as two prominent pioneers of the personal computer revolution."
            },
            new Contact()
            {
                Name = "Albert Einstein",
                Number = "+1 (702) ***-****",
                Favorite = true,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "German-born theoretical physicist[5] who developed the theory of relativity, one of the two pillars of modern physics (alongside quantum mechanics).[3][6]:274 His work is also known for its influence on the philosophy of science.[7][8] He is best known to the general public for his mass–energy equivalence formula E=mc^{2}}E = mc^2, which has been dubbed the world's most famous equation.[9] He received the 1921 Nobel Prize in Physics for his services to theoretical physics, and especially for his discovery of the law of the photoelectric effect,[10] a pivotal step in the development of quantum theory."
            },
            new Contact()
            {
                Name = "Marie Curie",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "Polish and naturalized-French physicist and chemist who conducted pioneering research on radioactivity. She was the first woman to win a Nobel Prize, the first person and the only woman to win the Nobel prize twice, and the only person to win the Nobel Prize in two different scientific fields. She was part of the Curie family legacy of five Nobel Prizes. She was also the first woman to become a professor at the University of Paris,[4] and in 1995 became the first woman to be entombed on her own merits in the Panthéon in Paris.[5]"
            },
            new Contact()
            {
                Name = "Isaac Newton",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "English mathematician, physicist, astronomer, theologian, and author (described in his own day as a 'natural philosopher') who is widely recognised as one of the most influential scientists of all time and as a key figure in the scientific revolution. His book Philosophiæ Naturalis Principia Mathematica (Mathematical Principles of Natural Philosophy), first published in 1687, laid the foundations of classical mechanics. Newton also made seminal contributions to optics, and shares credit with Gottfried Wilhelm Leibniz for developing the infinitesimal calculus."
            },
            new Contact()
            {
                Name = "Louis Pasteur",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "French biologist, microbiologist and chemist renowned for his discoveries of the principles of vaccination, microbial fermentation and pasteurization. He is remembered for his remarkable breakthroughs in the causes and prevention of diseases, and his discoveries have saved many lives ever since. He reduced mortality from puerperal fever, and created the first vaccines for rabies and anthrax."
            },
            new Contact()
            {
                Name = "Stephen Hawking",
                Number = "+1 (702) ***-****",
                Favorite = true,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "English theoretical physicist, cosmologist, and author who was director of research at the Centre for Theoretical Cosmology at the University of Cambridge at the time of his death.[18][19][8] He was the Lucasian Professor of Mathematics at the University of Cambridge between 1979 and 2009."
            },
            new Contact()
            {
                Name = "Blaise Pascal",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "French mathematician, physicist, inventor, writer and Catholic theologian. He was a child prodigy who was educated by his father, a tax collector in Rouen. Pascal's earliest work was in the natural and applied sciences, where he made important contributions to the study of fluids, and clarified the concepts of pressure and vacuum by generalising the work of Evangelista Torricelli. Pascal also wrote in defence of the scientific method."
            },
            new Contact()
            {
                Name = "Thomas Edison",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "American inventor and businessman who has been described as America's greatest inventor.[1][2][3] He developed many devices in fields such as electric power generation, mass communication, sound recording, and motion pictures.[4] These inventions, which include the phonograph, the motion picture camera, and the long-lasting, practical electric light bulb, have had a widespread impact on the modern industrialized world.[5] He was one of the first inventors to apply the principles of organized science and teamwork to the process of invention, working with many researchers and employees. He established the first industrial research laboratory.[6]"
            },
            new Contact()
            {
                Name = "Michel Faraday",
                Number = "+1 (702) ***-****",
                Favorite = true,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "English scientist who contributed to the study of electromagnetism and electrochemistry. His main discoveries include the principles underlying electromagnetic induction, diamagnetism and electrolysis."
            },
            new Contact()
            {
                Name = "Nikola Tesla",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "Serbian-American[4][5][6] inventor, electrical engineer, mechanical engineer, and futurist who is best known for his contributions to the design of the modern alternating current (AC) electricity supply system.[7]"
            },
            new Contact()
            {
                Name = "Charles Darwin",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "English naturalist, geologist and biologist,[6] best known for his contributions to the science of evolution.[I] His proposition that all species of life have descended over time from common ancestors is now widely accepted, and considered a foundational concept in science.[7] In a joint publication with Alfred Russel Wallace, he introduced his scientific theory that this branching pattern of evolution resulted from a process that he called natural selection, in which the struggle for existence has a similar effect to the artificial selection involved in selective breeding.[8] Darwin has been described as one of the most influential figures in human history,[9] and he was honoured by burial in Westminster Abbey.[10]"
            },
            new Contact()
            {
                Name = "Max Planck",
                Number = "+1 (702) ***-****",
                Favorite = true,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "German theoretical physicist whose discovery of energy quanta won him the Nobel Prize in Physics in 1918.[4] The Canadian Patriot publication describes Max Planck as a 'classical pianist and revolutionary physicist'. [5]"
            },
            new Contact()
            {
                Name = "Leonard de Vinci",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "Italian polymath of the Renaissance whose areas of interest included invention, drawing, painting, sculpture, architecture, science, music, mathematics, engineering, literature, anatomy, geology, astronomy, botany, paleontology, and cartography. He has been variously called the father of palaeontology, ichnology, and architecture, and is widely considered one of the greatest painters of all time (despite perhaps only 15 of his paintings having survived).[b]"
            },
            new Contact()
            {
                Name = "Heinrich Hertz",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "German physicist who first conclusively proved the existence of the electromagnetic waves predicted by James Clerk Maxwell's equations of electromagnetism. The unit of frequency, cycle per second, was named the 'hertz' in his honor.[3]"
            },
            new Contact()
            {
                Name = "Georg Ohm",
                Number = "+1 (702) ***-****",
                Favorite = true,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "German physicist and mathematician. As a school teacher, Ohm began his research with the new electrochemical cell, invented by Italian scientist Alessandro Volta. Using equipment of his own creation, Ohm found that there is a direct proportionality between the potential difference (voltage) applied across a conductor and the resultant electric current. This relationship is known as Ohm's law."
            },
            new Contact()
            {
                Name = "James Watt",
                Number = "+1 (702) ***-****",
                Favorite = true,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "Scottish inventor, mechanical engineer, and chemist who improved on Thomas Newcomen's 1712 Newcomen steam engine with his Watt steam engine in 1776, which was fundamental to the changes brought by the Industrial Revolution in both his native Great Britain and the rest of the world."
            }
        };
            InitializeComponent();
            BindingContext = this;
        }
        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set
            {
                isRefreshing = value;
                OnPropertyChanged();
            }
        }

        async Task RefreshItemsAsync()
        {
            IsRefreshing = true;
            await Task.Delay(TimeSpan.FromSeconds(RefreshDuration));
            Contacts.Add(new Contact
            {
                Name = "Steve Jobs",
                Number = "+1 (702) ***-****",
                Favorite = false,
                ImgContact = "baseline_account_circle_black_24dp.png",
                Bio = "American business magnate, industrial designer, investor, and media proprietor. He was the chairman, chief executive officer (CEO), and co-founder of Apple Inc., the chairman and majority shareholder of Pixar, a member of The Walt Disney Company's board of directors following its acquisition of Pixar, and the founder, chairman, and CEO of NeXT. Jobs is widely recognized as a pioneer of the personal computer revolution of the 1970s and 1980s, along with Apple co-founder Steve Wozniak."
            });
            IsRefreshing = false;
        }
        async void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            Contact tappedItem = e.Item as Contact; //Select item tapped
            if (tappedItem == null)
                return;
            await Navigation.PushModalAsync(new NavigationPage(new SelectedContact(tappedItem))); // Lunch the tappedItem Page
        }
    }
}

