﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PullToRefresh
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectedContact : ContentPage
    {
        public Contact SlctdContact { get; set; }
        public SelectedContact()
        {
            InitializeComponent();
        }
        public SelectedContact(Contact Contact)
        {
            InitializeComponent();
            SlctdContact = Contact;
            BindingContext = this;
        }
        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}