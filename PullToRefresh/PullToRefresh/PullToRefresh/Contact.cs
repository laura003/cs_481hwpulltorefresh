﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PullToRefresh
{
    public class Contact
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public string ImgContact { get; set; }
        public string Bio { get; set; }
        public bool Favorite { get; set; }
    }
}
